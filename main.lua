--SPINE !
local spine = require "spine-love.spine"

--LIBS!
require "lib.slither"
vector      =   require "lib.hump.vector"
camera      =   require "lib.hump.camera"
Gamestate   =   require "lib.hump.gamestate"


--KISS!
require "class.engine.AABB"
require "class.engine.Color"
require "class.engine.DrawableObject"
require "class.engine.ImageDrawable"
require "class.engine.RenderQueue"
require "class.engine.InputManager"
require "class.engine.MouseManager"
require "class.engine.Entity"
require "class.engine.EntityManager"
require "class.engine.Clickable"

require "class.todolist"

require "class.gameplay.Buddy"
require "class.gameplay.BloodSplatter"
require "class.gameplay.Bomb"
require "class.gameplay.Wave"

function love.load()
    print( todolist )
    assert( love.graphics.isSupported( "canvas" ), "Your 3d card is too old to play this game" )
    maincam =   camera(love.graphics.getWidth()/2, love.graphics.getHeight()/2, 1, 0)
    mouse   =   MouseManager()
    rq      =   RenderQueue()
    bloodrq =   RenderQueue()
    bloodtx =   love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
    local x, y  =   maincam:worldCoords( 0, 0 )
    bloodtx_pos =   vector( x, y )
    rq:registerCamera( maincam )
    mouse:registerCamera( maincam )
    love.graphics.setFont(love.graphics.newFont(24))

    img   =     love.graphics.newImage( "content/loveicon.png" )
    sounds  =   {}
    for i=1, 4 do
        local s =   love.audio.newSource("content/sound/kill"..i..".wav", "static")
        table.insert( sounds, s )
    end
    target=     vector(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
    testTable   =   {}
    buddygroup  =   EntityManager()
    SCORE       =   0
    MAXBUDDIES  =   10
    createBuddies()

    --Instanciating a bomb
    bomb    =   Bomb()
    bomb:registerToQueue( rq )
    bomb:registerToInputManager( mouse, "released", "r" )
    buddygroup:addEntity( bomb )
    bomb.entityManager    =   buddygroup

    --Instanciating a wave
    wave    =   Wave()
    wave:registerToQueue( rq )
    buddygroup:addEntity( wave )
    wave.entityManager  =   buddygroup
    wave:setCanvas( bloodtx )

    love.graphics.setCanvas( bloodtx )
    love.graphics.setColor( 255, 255, 255, 255 )
    drawBackground()
    love.graphics.setCanvas()
end

function love.update( dt )
    mouse:update( dt )
    buddygroup:update( dt )
    createBuddies()
end

function love.draw()
    bloodrq:renderToTexture( bloodtx )
    love.graphics.draw( bloodtx, 0, 0 )

    rq:draw()
    love.graphics.setColor(0,0,0)
    love.graphics.print( ("Kill things ! Right and left clicks and spacebar !\nYou killed %d things so far!"):format(SCORE), 10, 10)
end

function love.keyreleased( key )
    if key == " " then
        print("clearing canvas")
        --clearCanvas( dt )
        wave:trigger()
    end
end

function love.mousepressed(x, y, key)
    mouse:mousepressed( x, y, key )
end

function love.mousereleased(x, y, key)
    mouse:mousereleased(x, y, key)
end

function love.quit()
end

function clamp( input, pMin, pMax )
    min =   pMin or 0
    max =   pMax or 1
    if input < min then
        return min
    elseif input > max then
        return max
    else
        return input
    end
end

function createBuddies()
    local x =   love.graphics.getHeight()
    local y =   love.graphics.getWidth()
    for i=1, (MAXBUDDIES - buddygroup:getCount() ) do
        local b =   Buddy()
        b.bounds    =   screenAABB
        b:setTarget( target )
        b:loadImage( "content/loveicon.png" )
        b:registerToQueue( rq )
        b:registerToInputManager( mouse, "pressed" )
        b.position  =   vector( math.random( -x, x ), math.random( -y, y ) )
        b.defaultSpeed     =   200
        b.number    =   i
        b:setKillSound( sounds[math.random(#sounds)] )
        b.bloodRenderQueue  =   bloodrq
        buddygroup:addEntity( b )
        b.entityManager    =   buddygroup
    end
end


function clearCanvas( dt )
    love.graphics.setCanvas( bloodtx )
    love.graphics.setColor( 255, 255, 255, 255 )
    drawBackground()
    --love.graphics.rectangle( "fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight() )
    love.graphics.setCanvas()
end

function drawBackground()
    if not tileimg then
        tileimg   =     love.graphics.newImage( "content/tile.jpg" )
    end
    local w =   tileimg:getWidth()
    local h =   tileimg:getHeight()
    local wo=   0
    local ho=   0
    while wo < love.graphics.getWidth() do
        ho  =   0
        while ho < love.graphics.getHeight() do
            love.graphics.draw( tileimg, wo, ho )
            ho  =   ho + h
        end
        wo  =   wo  + w
    end
end

Graphics    =   {}
function Graphics.setColor( pColor )
    assert( isinstance(pColor, Color ) or issubclass(pColor, Color) )
    love.graphics.setColor( pColor.r, pColor.g, pColor.b, pColor.a )
end
