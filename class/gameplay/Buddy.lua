------------------------------------------------------------------------
-- @class Buddy
------------------------------------------------------------------------

class "Buddy"( Entity, ImageDrawable, Clickable ){
}


function Buddy:__init__()
    Entity:__init__()
    ImageDrawable:__init__()
    Clickable:__init__()

    self.target =   nil
    self.speed  =   200
    self.bloodAmount    =   20
    self.bloodRenderQueue   =   nil
    self.entityManager  =   nil
    self.scale  =   {x=0.2, y=0.2}
    self.attached   =   true
    self.time       =   0
    self.lastFearTime   =   -1
    self.fearDuration   =   1
    self.lastDecisionTaken  =   -1
    self.decisionFrequency  =   1
    self.fearSpeed      =   210
    self.fearDistance   =   250
    self.defaultSpeed   =   200
    self.defaultColor   =   Color( 255, 255, 255, 255 )
    self.fearColor      =   Color( 255, 215, 215, 255 )
    self.radius         =   75
    self.deathDuration  =   0.6

    --Spine specific code
    self.skeleton       =   nil
    local json = spine.SkeletonJson.new()
    json.scale = 0.2
    local skeletonData  =   json:readSkeletonDataFile("content/animation/chicken.json")
    Buddy.walkAnimation  =   skeletonData:findAnimation("walk")
    Buddy.deathAnimation =   skeletonData:findAnimation("death")
    Buddy.fearAnimation  =   skeletonData:findAnimation("fear")
    self.skeleton       =   spine.Skeleton.new(skeletonData)
    self.skeleton:setToBindPose()
    function self.skeleton:createImage ( p_Attachment )
        return love.graphics.newImage("content/animation/"..p_Attachment.name..".png")
    end
end

function Buddy:setKillSound( p_sound )
    self.killSound  =   p_sound
end

function Buddy:update( dt )
    self.time   =   self.time   +   dt

    if self.active then
        self:updateBehavior( dt )
        self:computeAABB()
    end

    if self.isFearing then
        self.color  =   self.fearColor
    else
        self.color  =   self.defaultColor
    end


    if self.visible then
        self.queue:addDrawableObject( self )
    end
    self:registerToInputManager()
    self:updateSkeleton( dt )
    if self.startDeath and (self.time - self.startDeath)>self.deathDuration then
        self.dead   =   true
    end
    self.position   =   self.position+(self.direction*self.speed * dt )
end

function Buddy:updateSkeleton( dt )
    self.skeleton.x =   self.position.x
    self.skeleton.y =   self.position.y
    if self.direction.x < 0 then
        self.skeleton.flipX  =   true
    else
        self.skeleton.flipX  =   false
    end
    if self.startDeath then
        self.deathAnimation:apply( self.skeleton, (self.time - self.startDeath), true )
    elseif self.isFearing then
        self.fearAnimation:apply( self.skeleton, self.time, true )
    else
        self.walkAnimation:apply( self.skeleton, self.time, true )
    end
    self.skeleton:updateWorldTransform()
end

function Buddy:draw()
    self.skeleton:draw()
end

function Buddy:updateBehavior( dt )
    if (self.time - self.lastFearTime)>self.fearDuration then--If no fear
        self.isFearing   =   false
        if (self.direction:len2() == 0) or (self.target:dist(self.position) >= 500 )
        and (self.time - self.lastDecisionTaken) > self.decisionFrequency then --If far from target AND more than 1sec of travelling
            self:computeDirection()
        end
    end
end

function Buddy:computeDirection()
    if self.target then
        self.direction  =   self.target - self.position
    else
        self.direction  =   vector( math.random( -10, 10 ), math.random( -10, 10 ) )
    end
    --Adding a bit of randomness in path
    self.direction:rotate_inplace( math.random( -math.pi/4, math.pi/4 ) )
    self.direction:normalize_inplace()
    self.lastDecisionTaken  =   self.time
    self.speed  =   self.defaultSpeed
end

function Buddy:setTarget( p_target )
    assert( vector.isvector( p_target ) )
    self.target =   p_target
end

function Buddy:testPoint( p_point )
    --return self.boundingBox:contains( p_point )
    return (self.position:dist(p_point)<self.radius)
end

function Buddy:onClick()
    if not self.startDeath then
        self:die()
    end
    return true
end

function Buddy:registerBloodCanvas( p_canvas )
    self.bloodcanvas    =   p_canvas
end

function Buddy:die()
    --self.dead   =   true
    self.startDeath =   self.time
    self.killSound:stop()
    self.killSound:rewind()
    self.killSound:play()
    if SCORE then SCORE = SCORE + 1 end

    --Emit blood
    local center=   self.position
    --1/3 blood is in the same direction with highspeed
    for i=1, (self.bloodAmount/3) do
        local b = BloodSplatter()
        b.position  =   center
        b.attached  =   self.attached
        local rotation  =   math.random() * math.pi / 3 --Rotate to a max of 45deg
        rotation    =   rotation - math.pi/6
        b.direction =   self.direction:rotated( rotation )
        b:registerToQueue( self.bloodRenderQueue )
        b.speed =   self.speed * math.random(15, 40)/10
        self.entityManager:addEntity( b )
    end

    --1/3 blood is everywhere at slow speed
    for i=1, (self.bloodAmount/3) do
        local b = BloodSplatter()
        b.position  =   center
        b.attached  =   self.attached
        local rotation  =   math.random() * 2 * math.pi
        rotation    =   rotation - math.pi/2
        b.direction =   self.direction:rotated( rotation )
        b:registerToQueue( self.bloodRenderQueue )
        b.speed =   self.speed * math.random(15, 30)/10
        b.lifeTime  =   b.lifeTime/2
        b.size  =   b.size/2
        self.entityManager:addEntity( b )
    end

    --2/3 droplets
    --TODO:realdroplets: bloodtrail won't cut it
    for i=1, (self.bloodAmount*2/3) do
        local b = BloodSplatter()
        b.position  =   center
        b.attached  =   self.attached
        local rotation  =   math.random() * math.pi / 3 --Rotate to a max of 45deg
        rotation    =   rotation - math.pi/6
        b.direction =   self.direction:rotated( rotation )
        b.size      =   0
        b:registerToQueue( self.bloodRenderQueue )
        b.position  =   b.position + (b.speed*b.direction*math.random(50))
        b.trailing  =   false
        self.entityManager:addEntity( b )
    end

    --1/3 droplets
    --TODO:realdroplets: bloodtrail won't cut it
    for i=1, (self.bloodAmount/3) do
        local b = BloodSplatter()
        b.position  =   center
        b.attached  =   self.attached
        local rotation  =   math.random() * 2 * math.pi
        rotation    =   rotation - math.pi/2
        b.direction =   self.direction:rotated( rotation )
        b.size      =   0
        b:registerToQueue( self.bloodRenderQueue )
        b.position  =   b.position + (b.speed*b.direction*math.random(25))
        b.trailing  =   false
        self.entityManager:addEntity( b )
    end

    self.entityManager:trigger("fear", self.position )
end

function Buddy:fear( p_position )
    assert( p_position )
    if (p_position:dist(self.position) <= self.fearDistance ) then
        self.direction  =    self.position - p_position
        self.direction:normalize_inplace()
        self.speed  =   self.fearSpeed
        self.lastFearTime   =   self.time
        self.lastDecisionTaken  =   self.time
        self.isFearing  =   true
    end

end

function Buddy:explode( p_position, p_radius )
    assert( p_position )
    if(p_position:dist(self.position) <= p_radius) then
        self.direction  =   self.position - p_position
        self.direction:normalize_inplace()
        self.speed  =   self.fearSpeed
        self:die()
    end
end
