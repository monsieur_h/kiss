------------------------------------------------------------------------
-- @class Wave
------------------------------------------------------------------------

class "Wave"( Entity, ImageDrawable ){
}

function Wave:__init__()
    Entity:__init__()
    ImageDrawable:__init__()
    Clickable:__init__()

    self.duration   =   3
    self.speed  =   love.graphics.getWidth()/self.duration
    self.lastSendTime   =   0
    self.time   =   0
    self.entityManager  =   nil
    self.image  =   nil
    self.canvas =   nil
    self.direction  =   vector(-1, 0)
    self.defaultPosition    =   vector(love.graphics.getWidth(), 0 )
    self.position           =   self.defaultPosition:clone()
    self.image  =   love.graphics.newImage( "content/wave.png" )
    self.state  =   "idle"
    self.color  =   Color( 0, 0, 190, 100 )
    self.zindex =   100
    self.visible=   true
    --self.active =   false
    self.fadingTime =   0.6
    self.alpha  =   self.color.a
    self.lastTriggeredTime  =   0
    self.sound  =   love.audio.newSource("content/sound/wave.wav", "static")
end

function Wave:update( dt )
    self.time   =   self.time + dt
    if self.visible then
        self.queue:addDrawableObject( self )
    end
    if self.state == "running" then
        self.position   =   self.position + self.direction * self.speed * dt
        if self.canvas then
            love.graphics.setScissor( self.position.x+self.image:getWidth()/2, self.position.y, love.graphics.getWidth(), love.graphics.getHeight() )
            clearCanvas()
            love.graphics.setScissor()
        end
        if self.position.x < -self.image:getWidth() then
            self.state  =   "fading"
            self.fadingtStart    =   self.time
        end
    elseif self.state == "fading" then
        self.alpha  =  (self.time - self.fadingtStart) / self.fadingTime
        self.alpha  =   100 - (self.alpha * 255)
        self.color.a=   self.alpha
        self.color:clamp_inplace()
        if self.color.a == 0 then
            self.status = "idle"
        end
    end
end

function Wave:trigger()
print(self.time-self.lastTriggeredTime)
    if (self.time-self.lastTriggeredTime)>self.duration then
        self.position   =   self.defaultPosition:clone()
        self.state  =   "running"
        self.active =   true
        self.alpha  =   100
        self.color.a=   100
        self.sound:stop()
        self.sound:rewind()
        self.sound:play()
        self.lastTriggeredTime  =   self.time
    end
end

function Wave:setCanvas( p_canvas )
    assert( p_canvas )
    self.canvas =   p_canvas
end

function Wave:draw()
    --TODO:vertical loop
    local h =   self.image:getHeight()
    local w =   self.image:getWidth()
    local wo=   0
    while wo < love.graphics.getHeight() do
        love.graphics.draw( self.image, self.position.x, self.position.y+wo)
        wo  =   wo + h
    end
    love.graphics.rectangle( "fill", self.position.x+self.image:getWidth(), self.position.y, love.graphics.getWidth(), love.graphics.getHeight() )
end


