------------------------------------------------------------------------
-- @class Bomb
------------------------------------------------------------------------

class "Bomb"( Entity, Clickable, DrawableObject ){
}

function Bomb:__init__( p_position )
    Entity:__init__()
    Clickable:__init__()

    self.position   =   p_position or vector()
    self.time       =   0
    self.countdownDuration  =   4
    self.explosionDuration  =   0.8
    self.radius     =   200
    self.color      =   Color( 25, 255, 25, 255 )
    self.backColor  =   Color( 0, 255, 0, 155 )
    self.state      =   "boom"
    self.active     =   true
    self.visible    =   false
    self.listenedButtons    =   {"r"}
    self.shockWaveSent  =   false
    self.lineWidth      =   2
    self.boomSound      =   love.audio.newSource("content/sound/boom.wav", "static")
    self.bipSound       =   love.audio.newSource("content/sound/bip.wav", "static")
end

function Bomb:update( dt )
    self.time   =   self.time + dt
    if self.state == "countdown" then
        if self.lastBip ~= math.floor(self.time - self.countdownDuration) then
            self:playBip()
            self.lastBip    =   math.floor( self.time - self.countdownDuration )
        end
    end
    if (self.time - self.countdownDuration)>0 and self.state == "countdown" then
        self.state  =   "boom"
        if not self.shockWaveSent then
            self:playBoom()
            self.entityManager:trigger( "explode", self.position, self.radius )
            print("SENDING SHOCKWAVE")
            self.shockWaveSent  =   true
        end

    elseif self.state == "boom" and (self.time-self.countdownDuration)>self.explosionDuration then
        self.visible  =   false
    end

    if self.visible then
        self.queue:addDrawableObject( self )
    end
    self:registerToInputManager()
end

function Bomb:draw()
    if self.state == "countdown" then
        love.graphics.setLine( self.lineWidth )
        local eta   =   self.countdownDuration-self.time
        love.graphics.print( ("Impact in : %d"):format(eta+1), self.position.x+10, self.position.y+10 )

        Graphics.setColor( self.backColor )
        --Target circle
        love.graphics.circle( "line", self.position.x, self.position.y, self.radius, 50 )

        --Actual circle
        Graphics.setColor( self.color )
        love.graphics.circle( "line", self.position.x, self.position.y, self.radius + eta*10, 50 )

        love.graphics.line( self.position.x + 10, self.position.y, self.position.x - 10, self.position.y )
        love.graphics.line( self.position.x, self.position.y - 10, self.position.x, self.position.y + 10 )
    else
        local eta   =   self.time-self.countdownDuration
        local ratio =   eta * -1 / self.explosionDuration
        love.graphics.setColor( 255, 255, 255, 255 * ratio )
        love.graphics.circle( "fill", self.position.x, self.position.y, self.radius, 40 )
        love.graphics.setColor( 255, 255, 255, 255 )
    end
end

function Bomb:trigger()
    print("bomb triggered")
    self.time       =   0
    self.position   =   self.input.releasedPos
    --self.active     =   true
    self.visible    =   true
    self.state      =   "countdown"
    self.shockWaveSent  =   false
end

function Bomb:testPoint()
    return true
end

function Bomb:onClick()
    self:trigger()
end

function Bomb:playBip()
    self.bipSound:stop()
    self.bipSound:rewind()
    self.bipSound:play()
end

function Bomb:playBoom()
    self.boomSound:stop()
    self.boomSound:rewind()
    self.boomSound:play()
end
