------------------------------------------------------------------------
-- @class BloodSplatter
------------------------------------------------------------------------

class "BloodSplatter"( Entity, ImageDrawable ){
}

function BloodSplatter:__init__()
    self.color  =   Color( math.random( 90, 155), 0, 0, math.random(150, 255) )
    self.time   =   0
    self.speed  =   2
    self.lifeTime   =   math.random( 1, 3 ) / 10
    self.friction   =   0.9
    self.size       =   math.random( 2, 10 )
    self.zindex     =   math.random( 1, 100)
    self.trailing   =   true
end

function BloodSplatter:draw()
    love.graphics.circle( "fill", self.position.x, self.position.y, self.size, 10)
    if self.previousPosition and self.trailing then --Interpolation since last frame
        local d =   self.position:dist(self.previousPosition)
        d   =   math.abs(d)
        d   =   clamp(10, 25)
        for i=1, d do
            local interPos  =   self.previousPosition + self.direction * i
            love.graphics.circle( "fill", interPos.x, interPos.y, self.size, 5)
        end
    end
end

function BloodSplatter:update( dt )
    self.time   =   self.time + dt
    self.speed  =   self.speed - (self.speed*self.friction) * dt
    self.size   =   self.size + ( math.random(10)-6.6)
    self.size   =   clamp( self.size, 0, 4 )
    --self.color.r=   self.color.r - (math.random(1, 9))
    --self.color.r=   clamp( self.color.r, 0, 255 )

    if self.active then
        self.position   =   self.position+(self.direction*self.speed*dt)
        self.queue:addDrawableObject( self )
    end
    self.previousPosition   =   self.position:clone()
    if self.time > self.lifeTime then
        self.active =   false
        self.dead   =   true
    end
end
