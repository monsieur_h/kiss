------------------------------------------------------------------------
-- @class InputManager
------------------------------------------------------------------------

class "InputManager"{
}

function InputManager:__init__()
    self.listeners  =   {pressed={}, released={}}
end

function InputManager:addListener( p_element, p_event )
    local reg   =   p_event or "released"
    self.listeners[reg][#self.listeners[reg]+1]   =   p_element
    --print(("Listener for %s was added, total sum: %d"):format(reg, #
end

function InputManager:trigger( p_event, p_ScreenPos, p_WorldPos, button )
    assert( self.listeners[p_event] )
    assert( p_event and p_ScreenPos )
    --print( ("Triggering %s event for button %s on %d elements"):format(p_event, button, #self.listeners[p_event]))
    local clickHandled  =   false
    local pWorld        =   p_WorldPos or p_ScreenPos
    local pScreen       =   p_ScreenPos
    local i =   1
    while ( i<=#self.listeners[p_event] and (not clickHandled) ) do
        if self.listeners[p_event][i].attached and self.listeners[p_event][i]:testPoint(pWorld) then
            if self.listeners[p_event][i]:handlesButton( button ) then
                clickHandled    =   self.listeners[p_event][i]:onClick()
            end
        end

        if not self.listeners[p_event][i].attached and self.listeners[p_event][i]:testPoint(pScreen) then
            if self.listeners[p_event][i]:handlesButton( button ) then
                clickHandled    =   self.listeners[p_event][i]:onClick()
            end
        end
        i   =   i + 1
    end
end

function InputManager:clearListeners()
    --TODO:Make a clean loop on this
    self.listeners.pressed  =   {}
    self.listeners.released =   {}
end
