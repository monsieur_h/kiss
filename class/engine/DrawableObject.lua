------------------------------------------------------------------------
-- @class DrawableObject
-- ABSTRACT CLASS
------------------------------------------------------------------------

class "DrawableObject" {
}

function DrawableObject:__init__()
    self.color      =   Color(255, 255, 255, 255)
    self.position   =   vector( 0, 0 )
    self.visible    =   true
    self.zindex     =   0 --Range is -1000, 1000
    self.boundingBox=   AABB()
    self.attached   =   true
    self.queue      =   nil
end

function DrawableObject:draw() --ABSTRACT : implement in children classes
end

function DrawableObject:computeAABB()--ABSTRACT : implement in children classes
end

function DrawableObject:update( dt )
    if self.visible and self.queue then
        self.queue:addDrawableObject( self )
    end
    self:computeAABB()
end

function DrawableObject:registerToQueue( p_target )
    assert( p_target and isinstance(p_target, RenderQueue) )
    self.queue  =   p_target
end
