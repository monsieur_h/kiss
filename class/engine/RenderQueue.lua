------------------------------------------------------------------------
-- @class RenderQueue
-- Renders a list of drawable to the screen
------------------------------------------------------------------------

class "RenderQueue"{
}

function RenderQueue:__init__()
    self.queue          =   {}
    self.camera         =   nil
    self.cameraAABB     =   nil
    self.attached       =   false
    self.screenAABB     =   AABB(0, 0, love.graphics.getWidth(), love.graphics.getHeight() )
end

function RenderQueue:addDrawableObject( p_element )
    assert( p_element )
    assert( isinstance(p_element, DrawableObject) or issubclass(p_element, DrawableObject) )
    table.insert( self.queue, p_element )
end

function RenderQueue:registerCamera( p_camera )
    assert( p_camera )
    self.camera =   p_camera
end

function RenderQueue:sort()
    table.sort( self.queue, function(a,b) return a.zindex<b.zindex end )
end

function RenderQueue:setCurrentColor( p_color )
    assert( p_color )
    love.graphics.setColor( p_color.r, p_color.g, p_color.b, p_color.a )
    self.currentColor   =   p_color
end

function RenderQueue:draw()
    local nb    =   #self.queue
    self:sort()
    --~ self:screenSpaceCulling()
    --~ print("Culled :"..nb-#self.queue.." items")
    self:resetCurrentColor()
    for i, v in ipairs( self.queue ) do
        if not ( self.currentColor == v.color ) then
            self:setCurrentColor( v.color )
        end
        if not ( self.attached == v.attached ) then
            self:setCameraAttached( v.attached )
        end
        v:draw()
    end
    self:clear()
end

function RenderQueue:registerCamera( p_camera )
    self.camera     =   p_camera
end

function RenderQueue:setCameraAttached( p_bool )
    if not self.camera then return end
    if p_bool then
        self.camera:attach()
        self.attached   =   true
    else
        self.camera:detach()
        self.attached   =   false
    end
end

function RenderQueue:clear()
    self.queue  =   {}
    love.graphics.setColor( 255, 255, 255, 255 ) if self.attached then
        self:setCameraAttached( false )
    end
end

function RenderQueue:getCameraAABB()
    if self.camera then
        if self.camera.rot == 0 then --already aligned, quick AABB
            local tlx, tly  =    self.camera:worldCoords( 0, 0 )
            local brx, bry  =   self.camera:worldCoords( love.graphics.getWidth(), love.graphics.getHeight() )
            return AABB( tlx, tly, brx, bry )
        else
            local tlx, tly  =    self.camera:worldCoords( 0, 0 )
            local brx, bry  =   self.camera:worldCoords( love.graphics.getWidth(), love.graphics.getHeight() )
            return AABB( math.min(tlx, brx), math.min(tly,bry), math.max(tlx, brx), math.max(tly,bry) )
        end
    end
end

function RenderQueue:screenSpaceCulling()
    --Removes elements that are not in the screen or in its projection
    local camBox    =   self:getCameraAABB()
    --Reverse loop idea/3loops ideas from:
    --http://stackoverflow.com/questions/12394841/safely-remove-items-from-an-array-table-while-iterating
    local new   =   {}
    local nc = 1
    for i=#self.queue,1,-1 do --Loop 1 : identify what to remove
        if self.queue[i].attached then
            if camBox:crosses( self.queue[i].boundingBox ) then
                new[nc] = self.queue[i]
                nc = nc + 1
            end
        else
            if v.boundingBox:crosses( screenAABB ) then
                new[nc] = self.queue[i]
                nc = nc + 1
            end
        end
    end
    self.queue = new
end

function RenderQueue:resetCurrentColor()
    local r, g, b ,a    =   love.graphics.getColor()
    self.currentColor   =   Color( r, g, b, a )
end

function RenderQueue:renderToTexture( p_canvas )
    love.graphics.setCanvas( p_canvas )
    love.graphics.setColor( 255, 255, 255, 255 )
    --print( ("rendering %d elements to canvas"):format(#self.queue) )
    self:draw()
    love.graphics.setColor( 255, 255, 255, 255 )
    love.graphics.setCanvas()
end
