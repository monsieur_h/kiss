------------------------------------------------------------------------
-- @class AABB
-- An axis aligned bounding box
------------------------------------------------------------------------

class "AABB"{
}

function AABB:__init__( p_minx, p_miny, p_maxx, p_maxy )
    self.topleft    =   vector( p_minx or 0, p_miny or 0 )
    self.bottomright=   vector( p_maxx or 0, p_maxy or 0 )
end

function AABB:contains( p_point )
    return ( p_point.x >= self.topleft.x
        and p_point.x <= self.bottomright.x
        and p_point.y >= self.topleft.y
        and p_point.y <= self.bottomright.y )
end

function AABB:getPoints() --Returns a vector for each point
    return vector( self.topleft.x, self.topleft.y ),
            vector( self.topleft.x, self.bottomright.y ),
            vector( self.bottomright.x, self.topleft.y ),
            vector( self.bottomright.x, self.bottomright.y )
end

function AABB:crosses( p_AABB ) --checks if two AABB overlaps
    assert( isinstance( p_AABB, AABB ) )
    --Do I contain an angle of p_AABB?
    local points    =   { p_AABB:getPoints() }
    for i, v in pairs( points ) do
        if self:contains( v ) then
           return true
        end
    end
    return false
end

function AABB:getWidth()
    return self.topleft.x - self.bottomright.x
end

function AABB:getHeight()
    return self.topleft.y - self.bottomright.y
end

function AABB:fromImage( p_img )
    assert( p_img )
    self.topleft    =   vector( 0, 0 )
    self.bottomright=   vector( p_img:getWidth(), p_img:getHeight() )
end

function AABB:getCenter()
    return (self.topleft + self.bottomright)/2
end

function AABB:print()
    print (("AABB( %d %d %d %d )"):format( self.topleft.x, self.topleft.y, self.bottomright.x, self.bottomright.y))
end
