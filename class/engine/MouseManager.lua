------------------------------------------------------------------------
-- @class MouseManager
------------------------------------------------------------------------

class "MouseManager"( InputManager ) {
}

function MouseManager:__init__()
    InputManager:__init__()
    self.pointerPos     =   vector(0,0)
    self.pressedPos     =   vector()
    self.releasedPos    =   vector()
    self.timePressed    =   0
    self.camera         =   nil
    self.pressed        =   false
end

function MouseManager:getScreenPos()
    return pointerPos
end

function MouseManager:registerCamera( p_cam )
    assert( p_cam )
    self.camera =   p_cam
end

function MouseManager:getWorldPos()
    if self.camera then
        local x, y = self.camera:mousepos()
        return vector( x, y )
    else
        return self.pointerPos
    end
end

function MouseManager:update( dt )
    local x, y      =   love.mouse.getPosition()
    self.pointerPos =   vector( x, y )
    if self:isPressed() then
        self.timePressed    =   self.timePressed + dt
    end
    self:clearListeners()
end

function MouseManager:mousepressed( x, y, button )
    self.pressed            =   true
    self.pressedPos         =   vector( x, y )
    local wPos              =   self:getWorldPos()
    self:trigger( "pressed", self.pressedPos, wPos, button )
end

function MouseManager:mousereleased( x, y, button )
    self.pressed            =   false
    self.releasedPos        =   vector( x, y )
    local wPos              =   self:getWorldPos()
    self:trigger( "released", self.releasedPos, wPos, button )
end

function MouseManager:isPressed( any )
    if any then
        return love.mouse.isPressed( any )
    else
        return self.pressed
    end
end

function MouseManager:getDragVector()
    return (self.releasedPos - self.pressedPos)
end

function MouseManager:drawDebug()
    if self.pressed then
        love.graphics.setColor( 255, 0, 0, 255 )
        love.graphics.line( self.pointerPos.x, self.pointerPos.y, self.pressedPos:unpack() )
    end
    love.graphics.setColor( 255, 255, 255, 255 )
    love.graphics.circle( "fill", self.pointerPos.x, self.pointerPos.y, 30, 20 )
end
