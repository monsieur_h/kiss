------------------------------------------------------------------------
-- @class Entity
-- An object to manage (add remove, enable/disable) entities
------------------------------------------------------------------------

class "EntityManager"{
}

function EntityManager:__init__()
    self.innerList  =   {}
end

function EntityManager:update( dt )
    for i=#self.innerList, 1, -1 do
        if self.innerList[i].dead then
            table.remove( self.innerList, i )
        elseif self.innerList[i].active then
            self.innerList[i]:update( dt )
        end
    end
end

function EntityManager:addEntity( p_entity )
    assert( p_entity )
    assert( isinstance(p_entity, Entity) or issubclass(p_entity, Entity) )
    table.insert( self.innerList, p_entity )
end

function EntityManager:getCount()
    return #self.innerList
end

function EntityManager:trigger( func, ...)
    for i,v in pairs(self.innerList) do
        if v[func] then
            v[func](v, ...)
        end
    end
end
