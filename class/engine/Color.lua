------------------------------------------------------------------------
-- @class Color
------------------------------------------------------------------------

class "Color"{
}

function Color:__init__( r, g, b, a )
    self.r  =   r or 0
    self.g  =   g or 0
    self.b  =   b or 0
    self.a  =   a or 0
    self:clamp_inplace()
end

function Color:__mul__( p_color )
    assert( isinstance( p_color, Color ) )
    return Color( self.color.r * p_color.r,
                    self.color.g * p_color.g,
                    self.color.b * p_color.b,
                    self.color.a * p_color.a )
end

function Color:__add__( p_color )
    assert( isinstance( p_color, Color ) )
    return Color( self.color.r + p_color.r,
                    self.color.g + p_color.g,
                    self.color.b + p_color.b,
                    self.color.a + p_color.a )
end

function Color:__sub__( p_color )
    assert( isinstance( p_color, Color ) )
    return Color( self.color.r - p_color.r,
                    self.color.g - p_color.g,
                    self.color.b - p_color.b,
                    self.color.a - p_color.a )
end

function Color:__eq(op1, op2)
    return ( op1.r == op2.r 
            and op1.g == op2.g
            and op1.b == op2.b
            and op1.a == op2.a )
end

function Color:clamp_inplace()
    self.r  =   self:clamp_channel( self.r )
    self.g  =   self:clamp_channel( self.g )
    self.b  =   self:clamp_channel( self.b )
    self.a  =   self:clamp_channel( self.a )
end

function Color:clamp_channel( p_value )
    if p_value > 255 then
        return 255
    elseif p_value < 0 then
        return 0
    else
        return p_value
    end
end

-- Converts HSL to RGB. (input and output range: 0 - 255)
-- adapted from https://love2d.org/wiki/HSL_color
function Color:fromHSL(h, s, l, a)
    if s<=0 then return l,l,l,a end
        h, s, l = h/256*6, s/255, l/255
        local c = (1-math.abs(2*l-1))*s
        local x = (1-math.abs(h%2-1))*c
        local m,r,g,b = (l-.5*c), 0,0,0
        if h < 1     then r,g,b = c,x,0
        elseif h < 2 then r,g,b = x,c,0
        elseif h < 3 then r,g,b = 0,c,x
        elseif h < 4 then r,g,b = 0,x,c
        elseif h < 5 then r,g,b = x,0,c
        else              r,g,b = c,0,x
    end 
    self.r  =   (r+m)*255
    self.g  =   (g+m)*255
    self.b  =   (b+m)*255
    self.a  =   a
end
