------------------------------------------------------------------------
-- @class Clickable
-- ABSTRACT
------------------------------------------------------------------------

class "Clickable"{
}

function Clickable:__init__()
    self.attached   =   true
    self.clickable  =   true
    self.input      =   nil
    self.listenedButtons    =   {"l"}
end

function Clickable:testPoint( p_point )
    return false
end

function Clickable:registerToInputManager( p_input, p_eventString )
    if p_input then
        self.input  =   p_input
    end
    if p_eventString then
        self.eventstring    =   p_eventString
    end
    if self.input then
        self.input:addListener( self, self.eventstring )
    end
end

function Clickable:handlesButton( p_button )
    assert( p_button )
    for i=1, #self.listenedButtons do
        if p_button == self.listenedButtons[i] then
            return true
        end
    end
    return false
end
