------------------------------------------------------------------------
-- @class ImageDrawable
------------------------------------------------------------------------

class "ImageDrawable"( DrawableObject ){
}

function ImageDrawable:__init__()
    DrawableObject:__init__()
    self.image  =   nil
    self.offset =   vector(0, 0)
    self.scale  =   vector(1, 1)
    self.rotation=  0
end

function ImageDrawable:setImage( p_img )
    assert( p_img )
    self.image          =   p_img
    self.boundingBox:fromImage( self.image )
end

function ImageDrawable:loadImage( p_path )
    assert( p_path )
    if love.filesystem.exists( p_path ) then
        local img   =   love.graphics.newImage( p_path )
        self:setImage( img )
    else
        error( "The file doesn't exist in the LOVE filesystem" )
    end
end

function ImageDrawable:draw()
    love.graphics.draw( self.image, self.position.x, self.position.y, self.rotation, self.scale.x, self.scale.y, self.offset.x, self.offset.y )
end

function ImageDrawable:computeAABB()
    self.boundingBox    =   AABB(self.position.x, self.position.y, self.position.x+self.image:getWidth()*self.scale.x, self.position.y+self.image:getHeight()*self.scale.y )
end

function ImageDrawable:update( dt )
    if self.queue then
        self.queue:addDrawableObject( self )
    end
end
