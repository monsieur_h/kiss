------------------------------------------------------------------------
-- @class Entity
-- ABSTRACT CLASS
-- A typical world entity, that can move around, with no specific behavior
------------------------------------------------------------------------

class "Entity"{
}

function Entity:__init__()
    self.direction  =   vector( 0, 0 )
    self.speed      =   0
    self.active     =   true
end

function Entity:updateBehavior( dt )-- ABSTRACT: place behavior in here
end

function Entity:update( dt )
    self:updateBehavior( dt )
end
